<?php

namespace Drupal\jsonapi_flag\EventSubscriber;

use Drupal\flag\Event\FlaggingEvent;
use Drupal\flag\Event\UnflaggingEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FlagEventsSubscriber.
 */
class FlagEventsSubscriber implements EventSubscriberInterface
{

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    $events['flag.entity_flagged'][] = ['flagEntityFlagged'];
    $events['flag.entity_unflagged'][] = ['flagEntityUnflagged', -100];

    return $events;
  }

  /**
   * Method is called whenever the flag.entity_flagged event is dispatched.
   *
   * @param \Drupal\flag\Event\FlaggingEvent $event
   *   Event.
   */
  public function flagEntityFlagged(FlaggingEvent $event)
  {

    $entity_type_id = $event->getFlagging()->getFlaggableType();
    $id = $event->getFlagging()->getFlaggableId();

    $product = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($id);

    // Invalidate cache.
    $cache_tags = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($id)->getCacheTags();
    \Drupal::service('cache_tags.invalidator')->invalidateTags($cache_tags);
  }

  /**
   * Method is called whenever the flag.entity_unflagged event is dispatched.
   *
   * @param \Drupal\flag\Event\UnflaggingEvent $event
   *   Event.
   */
  public function flagEntityUnflagged(UnflaggingEvent $event)
  {

    $flaggings = $event->getFlaggings();

    /** @var \Drupal\flag\FlaggingInterface $flagging */
    foreach ($flaggings as $flagging) {
      $entity_type_id = $flagging->getFlaggableType();
      $id = $flagging->getFlaggableId();

      // Invalidate cache.
      $cache_tags = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($id)->getCacheTags();
      \Drupal::service('cache_tags.invalidator')->invalidateTags($cache_tags);
    }
  }

}
