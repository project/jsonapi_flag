<?php

namespace Drupal\jsonapi_flag\Plugin\Field;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;


class ComputedFlaggingsCurrentUser extends FieldItemList
{

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue()
  {
    $current_user = \Drupal::currentUser();
    if ($current_user->isAuthenticated()){
      $entity = $this->getEntity();
      $entity->addCacheContexts(['user']);

      /** @var \Drupal\flag\FlagService $flag_service */
      $flag_service = \Drupal::service('flag');

      $flaggings = $flag_service->getAllEntityFlaggings($entity, $current_user);

      foreach ($flaggings as $flagging) {
        $values[] = $flagging->getFlagId();
      }

      $this->list[0] = $this->createItem(0, $values);
    }


  }

}
