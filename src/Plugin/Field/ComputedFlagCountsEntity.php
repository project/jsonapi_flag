<?php

namespace Drupal\jsonapi_flag\Plugin\Field;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;


class ComputedFlagCountsEntity extends FieldItemList
{

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue()
  {
    $entity = $this->getEntity();

    // Get the counts of all flaggings on the entity.
    $flag_get_counts  = \Drupal::service('flag.count')->getEntityFlagCounts($entity);

    $this->list[0] = $this->createItem(0, $flag_get_counts);
  }

}
