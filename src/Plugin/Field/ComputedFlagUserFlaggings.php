<?php

namespace Drupal\jsonapi_flag\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Show the counts of the flags done by the current user.
 *
 * @package Drupal\jsonapi_flag\Plugin\Field
 */
class ComputedFlagUserFlaggings extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue()
  {
    $flag_count  = \Drupal::service('flag.count');

    $user = $this->getEntity();

    $flags = \Drupal::service('flag')->getAllFlags();
    // Get the counts of all flaggings on the entity.
    $counts = [];

    foreach ($flags as $flag) {
      $counts[$flag->id()] = (int) $flag_count->getUserFlagFlaggingCount($flag, $user);
    }

    $this->list[0] = $this->createItem(0, $counts);
  }

}
